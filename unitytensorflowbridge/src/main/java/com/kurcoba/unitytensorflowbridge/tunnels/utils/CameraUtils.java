package com.kurcoba.unitytensorflowbridge.tunnels.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.util.Log;
import android.util.Size;

import java.util.Arrays;
import java.util.Collections;


public class CameraUtils {


    public static int getCameraRotation(Activity activity, String cameraId) {

        final CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            cameraId = manager.getCameraIdList()[0]; //TODO Defaulting to camera id 0 for testing

            final CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

            final StreamConfigurationMap map =
                    characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            return characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);

        } catch (final CameraAccessException e) {
            Log.d("UNITY", "CameraUtils getCameraRotation CameraAccessException " + e.getMessage());
        } catch (final NullPointerException e) {
            Log.d("UNITY", "CameraUtils getCameraRotation NullPointerException " + e.getMessage());

        }
        return -1;
    }
}
