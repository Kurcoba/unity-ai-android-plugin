package com.kurcoba.unitytensorflowbridge.tunnels.classification;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.SystemClock;
import android.util.Log;

import com.kurcoba.unitytensorflowbridge.tunnels.utils.ImageUtils;

import java.io.ByteArrayOutputStream;

public class ClassificationThread implements Runnable {

    byte[] imageBytesToClassify;
    int[] imageIntValsToClassify;

    Classifier classifier;


    Bitmap bitmapToClassify;

    private volatile boolean isCalculating;
    private ClassificationThreadCallback callback;

    private int[] rgbBytes = null;
    private int previewWidth;
    private int previewHeight;
    private Bitmap rgbFrameBitmap;
    private Bitmap croppedBitmap;

    private Matrix frameToCropTransform;

    public ClassificationThread(Classifier classifier, ClassificationThreadCallback callback){
        isCalculating = false;
        this.classifier = classifier;
        this.callback = callback;

    }

    public ClassificationThread(byte[] imageToClassify, Classifier classifier, ClassificationThreadCallback callback){
        isCalculating = false;
        this.imageBytesToClassify = imageToClassify;
        this.classifier = classifier;
        this.callback = callback;

    }

    public ClassificationThread(int[] imageToClassify, Classifier classifier, ClassificationThreadCallback callback){
        isCalculating = false;
        this.imageIntValsToClassify = imageToClassify;
        this.classifier = classifier;
        this.callback = callback;

    }

    public ClassificationThread(Bitmap imageToClassify, Classifier classifier, ClassificationThreadCallback callback){
        isCalculating = false;
        this.bitmapToClassify = imageToClassify;
        this.classifier = classifier;
        this.callback = callback;

    }

    public synchronized  boolean isCalculating(){
        return isCalculating;
    }


    public void setBitmapToClassify(Bitmap bitmapToClassify) {
        this.bitmapToClassify = bitmapToClassify;
    }

    @Override
    public void run() {
        isCalculating = true;
        final long startTime = SystemClock.uptimeMillis();

       // ImageUtils.convertYUV420SPToARGB8888(imageBytesToClassify, previewWidth, previewHeight, rgbBytes);
        Log.d("Unity", "Kurcoba run rgbFrameBitmap == null " + (rgbFrameBitmap == null) );

        if(rgbFrameBitmap == null){
            return;
        }
        /* converting the the preview image into a cropped image ready for the classifier*/
        rgbFrameBitmap.setPixels(imageIntValsToClassify, 0, previewWidth, 0, 0, previewWidth, previewHeight);
        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);

//
//        if(bitmapToClassify != null){
//
//            callback.onResults(classifier.recognizeImage(bitmapToClassify));
//            isCalculating = false;
//
//            return;
////            ByteArrayOutputStream stream = new ByteArrayOutputStream();
////            bitmapToClassify.compress(Bitmap.CompressFormat.PNG, 100, stream);
////            imageBytesToClassify = stream.toByteArray();
////
////            //bitmapToClassify.recycle();
////            bitmapToClassify = null;
//        }
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inMutable = true;
//        Bitmap bmp = BitmapFactory.decodeByteArray(imageBytesToClassify, 0, imageBytesToClassify.length, options);

        callback.onResults(classifier.recognizeImage(croppedBitmap));

        Log.d("Unity", "Kurcoba classification time " + (SystemClock.uptimeMillis() - startTime));

        isCalculating = false;

    }

    public void setClassificationData(int previewWidth, int previewHeight, Bitmap rgbFrameBitmap, Bitmap croppedBitmap, Matrix frameToCropTransform) {

        Log.d("Unity", "Kurcoba setClassificationData " );

        this.previewWidth = previewWidth;
        this.previewHeight = previewHeight;
        this.rgbFrameBitmap = rgbFrameBitmap;
        this.croppedBitmap = croppedBitmap;
        this.frameToCropTransform = frameToCropTransform;

    }
}
