package com.kurcoba.unitytensorflowbridge.tunnels;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;

import com.google.gson.Gson;
import com.kurcoba.unitytensorflowbridge.tunnels.classification.ClassificationEvents;
import com.kurcoba.unitytensorflowbridge.tunnels.classification.ClassificationThread;
import com.kurcoba.unitytensorflowbridge.tunnels.classification.ClassificationThreadCallback;
import com.kurcoba.unitytensorflowbridge.tunnels.classification.Classifier;
import com.kurcoba.unitytensorflowbridge.tunnels.classification.TensorFlowImageClassifier;
import com.kurcoba.unitytensorflowbridge.tunnels.utils.CameraUtils;
import com.kurcoba.unitytensorflowbridge.tunnels.utils.ImageUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ClassificationTunnel {


    // These are the settings for the original v1 Inception model. If you want to
    // use a model that's been produced from the TensorFlow for Poets codelab,
    // you'll need to set IMAGE_SIZE = 299, IMAGE_MEAN = 128, IMAGE_STD = 128,
    // INPUT_NAME = "Mul", and OUTPUT_NAME = "final_result".
    // You'll also need to update the MODEL_FILE and LABEL_FILE paths to point to
    // the ones you produced.
    //
    // To use v3 Inception model, strip the DecodeJpeg Op from your retrained
    // model first:
    //
    // python strip_unused.py \
    // --input_graph=<retrained-pb-file> \
    // --output_graph=<your-stripped-pb-file> \
    // --input_node_names="Mul" \
    // --output_node_names="final_result" \
    // --input_binary=true
    private static final int INPUT_SIZE = 224;
    private static final int IMAGE_MEAN = 117;
    private static final float IMAGE_STD = 1;
    private static final String INPUT_NAME = "input";
    private static final String OUTPUT_NAME = "output";


    private static final String MODEL_FILE = "file:///android_asset/tensorflow_inception_graph.pb";
    private static final String LABEL_FILE =
            "file:///android_asset/imagenet_comp_graph_label_strings.txt";

    private Classifier classifier;

    private ClassificationThread classificationThread;
    private ClassificationThreadCallback classifierCallback;
    private List<ClassificationEvents> listeners = new ArrayList<ClassificationEvents>();

    private Gson gson;

    /**
     * Used to build a matrix that can transform the input image into
     * size that the classifier expects
     */
    private Integer sensorOrientation;
    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;
    private static final boolean MAINTAIN_ASPECT = true;

    /**
     * Used to copy the input image into the correct format using the matrix created above
     */
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;

    private int previewWidth, previewHeight;

    private Context context;

//    public ClassificationTunnel(Context context){
//
//        classifier =
//                TensorFlowImageClassifier.create(
//                        context,
//                        MODEL_FILE,
//                        LABEL_FILE,
//                        INPUT_SIZE,
//                        IMAGE_MEAN,
//                        IMAGE_STD,
//                        INPUT_NAME,
//                        OUTPUT_NAME);
//
//        InitCallback();
//
//    }

    public ClassificationTunnel(){

        Log.d("KURCOBA", "Kurcoba native ClassificationTunnel constructor " );

        gson = new Gson();


        InitCallback();

    }

    /**
     *
     * @param context
     * @param previewWidth the width of the input image
     * @param previewHeight
     */
    public void initialize(Context context, String cameraID, int previewWidth, int previewHeight){

        initClassifier(context);

        this.previewWidth = previewWidth;
        this.previewHeight = previewHeight;

        sensorOrientation = CameraUtils.getCameraRotation((Activity)context, cameraID) - getScreenOrientation(context);

        frameToCropTransform = ImageUtils.getTransformationMatrix(
                previewWidth, previewHeight,
                INPUT_SIZE, INPUT_SIZE,
                sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);


        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(INPUT_SIZE, INPUT_SIZE, Bitmap.Config.ARGB_8888);
    }


    protected int getScreenOrientation(Context context) {
        switch (((Activity)context).getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_90:
                return 90;
            default:
                return 0;
        }
    }

    public float getTestString(){
        return 77f;
    }

    public void initClassifier(Context context){
       // this.context = context;

        Log.d("KURCOBA", "Kurcoba native ClassificationTunnel initClassifier " );


        classifier =
                TensorFlowImageClassifier.create(
                        context,
                        MODEL_FILE,
                        LABEL_FILE,
                        INPUT_SIZE,
                        IMAGE_MEAN,
                        IMAGE_STD,
                        INPUT_NAME,
                        OUTPUT_NAME);
    }

    ClassificationTunnel(String modelFile, String labelFile, int imageStd, int imageMean, int inputSize){

        classifier =
                TensorFlowImageClassifier.create(
                        null,
                        modelFile,
                        labelFile,
                        inputSize,
                        imageMean,
                        imageStd,
                        INPUT_NAME,
                        OUTPUT_NAME);
        InitCallback();

    }

    ClassificationTunnel(String modelFile, String labelFile, int imageStd, int imageMean, int inputSize, String inputName, String outputName){

        classifier =
                TensorFlowImageClassifier.create(
                        null,
                        modelFile,
                        labelFile,
                        inputSize,
                        imageMean,
                        imageStd,
                        inputName,
                        outputName);
        InitCallback();

    }

    private void InitCallback(){

        Log.d("KURCOBA", "Kurcoba native ClassificationTunnel InitCallback " );


        classifierCallback = new ClassificationThreadCallback(){

            @Override
            public void onResults(List<Classifier.Recognition> results) {

                Log.d("KURCOBA", "Kurcoba natiove sending results " );

                for(ClassificationEvents listener: listeners){
                    listener.onClassifierResultsJsonReady(gson.toJson(results));
                }
            }
        };
    }




    /**
     * Starts a thread to assess the image using the already loaded model file.
     * Results are posted back to any registered listeners. If a new classification request
     * comes in while one is running it is discarded
     * @param imageBytes
     */
    public void classify(byte[] imageBytes){

        Log.d("KURCOBA", "Kurcoba native classify bytes recieved " );


        if(classificationThread != null && classificationThread.isCalculating()){
            return;
        }

        classificationThread =  new ClassificationThread(imageBytes, classifier, classifierCallback);
        classificationThread.setClassificationData(previewWidth, previewHeight, rgbFrameBitmap, croppedBitmap, frameToCropTransform);
        new Thread(classificationThread).start();
    }

    public void classify(int[] imageintVals){

        Log.d("KURCOBA", "Kurcoba native classify bytes recieved " );


        if(classificationThread != null && classificationThread.isCalculating()){
            return;
        }

        classificationThread =  new ClassificationThread(imageintVals, classifier, classifierCallback);
        classificationThread.setClassificationData(previewWidth, previewHeight, rgbFrameBitmap, croppedBitmap, frameToCropTransform);
        new Thread(classificationThread).start();
    }

    public void classify(Bitmap image){

        if(classificationThread != null && classificationThread.isCalculating()){
            return;
        }

//        for(ClassificationEvents listener: listeners){
//            listener.onClassifierResultsReady(new ArrayList<Classifier.Recognition>());
//        }
        classificationThread =  new ClassificationThread(image, classifier, classifierCallback);

        new Thread(classificationThread).start(); //.run();
    }



    public void addClassificationEventListener(ClassificationEvents listener){

        Log.d("KURCOBA", "Kurcoba native adding listener " + listener.getClass().getSimpleName() );

        if(listeners.contains(listener)){
            return;
        }

        listeners.add(listener);
    }

    public void removeClassificationEventListener(ClassificationEvents listener){
        if(!listeners.contains(listener)){
            return;
        }

        listeners.remove(listener);
    }


}
