package com.kurcoba.unitytensorflowbridge.tunnels.classification;


public interface ClassificationEvents{

    // void onClassifierResultsReady(List<Classifier.Recognition> results);
    void onClassifierResultsJsonReady(String resultsJson);

}