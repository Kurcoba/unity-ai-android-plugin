package com.kurcoba.unitytensorflowbridge.tunnels.classification;

import java.util.List;

public interface  ClassificationThreadCallback {

    void onResults(List<Classifier.Recognition> results);
}
